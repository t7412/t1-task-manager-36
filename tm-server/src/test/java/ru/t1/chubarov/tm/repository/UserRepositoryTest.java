package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.repository.IUserRepository;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.service.PropertyService;
import ru.t1.chubarov.tm.util.HashUtil;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 1;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private final String userLogin = "TestLogin";
    @NotNull
    private final String userEmail = "Test@mail.test";

    @SneakyThrows
    @Before
    public void initRepository() {
        propertyService = new PropertyService();
        userRepository = new UserRepository();
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash(HashUtil.salt(propertyService, "password"));
        user.setRole(Role.USUAL);
        userRepository.add(user);
    }

    @Test
    public void testFindByLogin() {
        Assert.assertEquals(userLogin, userRepository.findByLogin(userLogin).getLogin());
    }

    @Test
    public void testfindByEmail() {
        Assert.assertEquals(userLogin, userRepository.findByEmail(userEmail).getLogin());
    }

    @Test
    public void testisLoginExist() {
        Assert.assertTrue(userRepository.isLoginExist(userLogin));
        Assert.assertFalse(userRepository.isLoginExist("Login"));
    }

    @Test
    public void testisEmailExist() {
        Assert.assertTrue(userRepository.isEmailExist(userEmail));
        Assert.assertFalse(userRepository.isEmailExist("user@Email.org"));
    }

    @Test
    public void testProfile() {
        @NotNull final User user = userRepository.findByLogin(userLogin);
        @NotNull final String userId = userRepository.findByLogin(userLogin).getId();
        @NotNull final String newLogin = "new_login";
        @NotNull final String newPassword = "new_password";
        user.setFirstName("Ivanov");
        user.setLastName("Piter");
        user.setMiddleName("Vasilech");
        user.setLogin(newLogin);
        user.setPasswordHash(HashUtil.salt(propertyService, newPassword));
        Assert.assertNull(userId, userRepository.findByLogin(userLogin));
        Assert.assertEquals(userId, userRepository.findByLogin(newLogin).getId());
        Assert.assertEquals("Ivanov", userRepository.findByLogin(newLogin).getFirstName());
        Assert.assertEquals("Piter", userRepository.findByLogin(newLogin).getLastName());
        Assert.assertEquals("Vasilech", userRepository.findByLogin(newLogin).getMiddleName());
        Assert.assertEquals(HashUtil.salt(propertyService, newPassword), userRepository.findByLogin(newLogin).getPasswordHash());
    }

    @Test
    public void testSetLock() {
        @NotNull final User user = userRepository.findByLogin(userLogin);
        Assert.assertFalse(userRepository.findByLogin(userLogin).getLocked());
        user.setLocked(true);
        Assert.assertTrue(userRepository.findByLogin(userLogin).getLocked());
        user.setLocked(false);
        Assert.assertFalse(userRepository.findByLogin(userLogin).getLocked());
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
        userRepository.add(new User());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userRepository.getSize());
    }

    @Test
    public void testClear() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
        userRepository.clear();
        Assert.assertEquals(0, userRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemove() {
        Assert.assertNull(userRepository.remove(userRepository.findByLogin("emptyLogin")));
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
        @NotNull final User user = userRepository.findByLogin(userLogin);
        userRepository.remove(user);
        Assert.assertEquals(0, userRepository.getSize());
    }

}
