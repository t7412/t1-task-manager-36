package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.repository.ISessionRepository;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Session;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 2;

    @NotNull
    private final String userIdFirstSession = UUID.randomUUID().toString();

    @NotNull
    private final String userIdSecondSession = UUID.randomUUID().toString();

    @NotNull
    private final String firstSessionId = UUID.randomUUID().toString();

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @SneakyThrows
    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository();
        @NotNull final Session session1 = new Session();
        session1.setUserId(userIdFirstSession);
        session1.setDate(new Date());
        session1.setId(firstSessionId);
        sessionRepository.add(session1);
        sessionList.add(session1);
        @NotNull final Session session2 = new Session();
        session2.setUserId(userIdSecondSession);
        session2.setDate(new Date());
        sessionRepository.add(session2);
        sessionList.add(session2);
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
        sessionRepository.add(userIdFirstSession, new Session());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, sessionRepository.getSize());
        sessionRepository.add(userIdFirstSession, new Session());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 2, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemove() {
        Assert.assertNull(sessionRepository.removeOne(userIdFirstSession,sessionRepository.findOneById(userIdFirstSession, "empty_id")));
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
        sessionRepository.remove(sessionRepository.findOneByIndex(userIdFirstSession, 0));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveAll() {
        sessionRepository.removeAll(userIdFirstSession);
        Assert.assertEquals(1, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        sessionRepository.removeOneById(userIdFirstSession, "empty_sessionId");
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
        sessionRepository.removeOneById(userIdFirstSession, firstSessionId);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveOneByIndex() {
        sessionRepository.removeOneByIndex(userIdFirstSession, 0);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testExistsById() {
        Assert.assertTrue(sessionRepository.existsById(userIdFirstSession, sessionRepository.findOneByIndex(0).getId()));
        Assert.assertTrue(sessionRepository.existsById(userIdSecondSession, sessionRepository.findOneByIndex(1).getId()));
        Assert.assertFalse(sessionRepository.existsById(userIdFirstSession, "empty_id"));
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        Assert.assertNotNull(sessionRepository.findOneById(userIdFirstSession, firstSessionId));
    }

    @SneakyThrows
    @Test
    public void testFindOneByIdNegative() {
        Assert.assertNull(sessionRepository.findOneById(userIdFirstSession, "empty_id"));
    }

    @SneakyThrows
    @Test
    public void testFindOneByIndex() {
        Assert.assertNotNull(sessionRepository.findOneByIndex(userIdFirstSession, 0));
    }

    @Test
    public void testFindOAll() {
        @NotNull final List<Session> actualSessionList = sessionRepository.findAll(userIdFirstSession);
        Assert.assertEquals(1, actualSessionList.size());
    }

    @SneakyThrows
    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(1, sessionRepository.getSize(userIdFirstSession));
    }

}
