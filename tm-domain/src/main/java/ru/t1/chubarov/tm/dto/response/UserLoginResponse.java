package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractUserResponse {

    public UserLoginResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(@Nullable final User user, @Nullable final String token, @Nullable final Throwable throwable) {
        super(user, token, throwable);
    }

    public UserLoginResponse(@Nullable final String token) {
        super(token);
    }

}
